AutoSaveForm V 0.8
-------------------------------
Copyright (c) 2012 iSummation technologies (www.isummation.com)
Dual licensed under the MIT or GPL Version 2 licenses.

Overview
--------
A jQuery plugin that makes storing form field values to cookie easier to restore back after page load.

Depends
-------
	Require :
	jquery.js 
	jquery.cookie.js : jQuery cookie plugin

Usage
-----

//Enable cookie store for form
$("#frmautosave").autoSaveForm({'prefix':'uniquename'});
Provide prefix as any unique value that make cookie name unique from other.

//Extract value from cookie and fill up into form variables
$('#frmautosave').restoreValueFromCookie();

// Use to delete cookie variable.
$('#frmautosave').deleteFormCookie();


Plugin is useful when we want to remember form as user types in and to retrieve it back in case user has closed browser accidently.
