/*************************************************************************
	jquery.autosaveform.js
	Allow you to save form value into cookie variable as user change value and 
	can be restored from cookies to form field by single function call.

	Copyright (c) 2012, iSummation Technologies (http://www.isummation.com)
	Dual licensed under the MIT or GPL Version 2 licenses.
	
	A current version and some documentation is available at
		https://bitbucket.org/thecfguy/jquery-autosaveform-plugin

	$Version: 0.8$

	@depends: jquery.js
	@depends: jquery.cookie.js
*************************************************************************/
;(function($){
	$.autoSaveForm = function(el,options){
		var base = this;
		base.$form = $(el);
		base.form = el;
		base.$form.data("autoSaveForm",base);
		
		base.init = function(){
			base.options = $.extend({},$.autoSaveForm.defaultOptions, options);
			base.cookieName =base.options.prefix + base.form.name;
			var $form = base.$form;
			$form.find("input").each(function(){
				$(this).bind('change',base.saveValueToCookie);
			});
			$form.find("select").each(function(){
				$(this).bind('change',base.saveValueToCookie);
			});
			$form.find("textarea").each(function(){
				$(this).bind('change',base.saveValueToCookie);
			});
		};
		
		base.saveValueToCookie = function(){
			$.cookie(base.cookieName,base.$form.serialize());
		};
		
		base.restoreValueFromCookie = function(){
    		var $values = $.cookie(base.cookieName);
    		if($values){
	    		var arrVal = $values.split("&");
	    		for(var i in arrVal){
	    			var arrField = arrVal[i].split("=");
	    				base.$form.find("[name=" + arrField[0] + "]").each(function(){
	    				var _this = this;
	    				var dVal = decodeURIComponent(arrField[1].replace(/\+/g,' '));
	    				switch(_this.type){
	    					case 'radio':
	    					case 'checkbox':
	    						if($(_this).val() == dVal)
	    							_this.checked = true;
	    						break;
	    					case 'select-multiple':
	    						$(_this).find("[value=" + dVal + "]").attr("selected","true");
	    						break;
	    					default:	
	    						$(_this).val(dVal);							
	    				}
	    				
	    			});
	    		}
    		}
		};
		
		base.deleteFormCookie = function(){
			$.cookie(base.cookieName,null);
		};
		
		base.init();
	};
	
	$.autoSaveForm.defaultOptions = {
		prefix: ''			//Add to generate unique cookie per page/form
	};
	
 	$.fn.autoSaveForm=function(options){
    	return this.each(function(){
            (new $.autoSaveForm(this, options));
        });
  	};
  	$.fn.restoreValueFromCookie = function(){
  		this.data("autoSaveForm").restoreValueFromCookie();
  	};
  	$.fn.deleteFormCookie = function(){
  		this.data("autoSaveForm").deleteFormCookie();
  	};
  	$.fn.getautoSaveForm = function(){
  		return this.data("autoSaveForm");
    };
})(jQuery);
