/*************************************************************************
	jquery.autosaveform.js
	Allow you to save form value into cookie variable as user change value and 
	can be restored from cookies to form field by single function call.

	Copyright (c) 2012, iSummation Technologies (http://www.isummation.com)
	Dual licensed under the MIT or GPL Version 2 licenses.
	
	A current version and some documentation is available at
		https://bitbucket.org/thecfguy/jquery-autosaveform-plugin

	$Version: 0.8$

	@depends: jquery.js
	@depends: jquery.cookie.js
*************************************************************************/
(function(a){a.autoSaveForm=function(b,c){var d=this;d.$form=a(b);d.form=b;d.$form.data("autoSaveForm",d);d.init=function(){d.options=a.extend({},a.autoSaveForm.defaultOptions,c);d.cookieName=d.options.prefix+d.form.name;var b=d.$form;b.find("input").each(function(){a(this).bind("change",d.saveValueToCookie)});b.find("select").each(function(){a(this).bind("change",d.saveValueToCookie)});b.find("textarea").each(function(){a(this).bind("change",d.saveValueToCookie)})};d.saveValueToCookie=function(){a.cookie(d.cookieName,d.$form.serialize())};d.restoreValueFromCookie=function(){var b=a.cookie(d.cookieName);if(b){var c=b.split("&");for(var e in c){var f=c[e].split("=");d.$form.find("[name="+f[0]+"]").each(function(){var b=this;var c=decodeURIComponent(f[1].replace(/\+/g," "));switch(b.type){case"radio":case"checkbox":if(a(b).val()==c)b.checked=true;break;case"select-multiple":a(b).find("[value="+c+"]").attr("selected","true");break;default:a(b).val(c)}})}}};d.deleteFormCookie=function(){a.cookie(d.cookieName,null)};d.init()};a.autoSaveForm.defaultOptions={prefix:""};a.fn.autoSaveForm=function(b){return this.each(function(){new a.autoSaveForm(this,b)})};a.fn.restoreValueFromCookie=function(){this.data("autoSaveForm").restoreValueFromCookie()};a.fn.deleteFormCookie=function(){this.data("autoSaveForm").deleteFormCookie()};a.fn.getautoSaveForm=function(){return this.data("autoSaveForm")}})(jQuery)